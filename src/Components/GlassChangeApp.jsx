import React, { Component } from "react";
import { dataGlasses } from "../data/dataGlasses";
import Image from "./Image.jsx";

export default class GlassChangeApp extends Component {
  state = {
    url: "./glassesImage/v1.png",
    name: "GUCCI G8850U",
    desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    display: "none",
  };
  handleChangeGlasses = (item) => {
    this.setState({
      url: `${item.url}`,
      name: `${item.name}`,
      desc: `${item.desc}`,
      display: "block",
    });
  };
  renderGlassesButton = () => {
    return dataGlasses.map((item) => {
      return (
        <div key={item.id} className="col-2">
          <img
            className="w-100"
            src={item.url}
            alt=""
            onClick={() => {
              this.handleChangeGlasses(item);
            }}
          />
        </div>
      );
    });
  };
  render() {
    return (
      <div className="container py-5">
        <Image data={this.state} />
        <div className="glasses-buttons row py-3 mt-5">
          {this.renderGlassesButton()}
        </div>
      </div>
    );
  }
}
