import React, { Component } from "react";

export default class Image extends Component {
  render() {
    return (
      <div>
        <div className="image d-flex justify-content-evenly">
          <div className="glasses-model">
            <img src="./glassesImage/model.jpg" alt="" />
          </div>
          <div className="glasses-model">
            <img src="./glassesImage/model.jpg" alt="" />
            <div
              className="glasses"
              style={{ display: this.props.data.display }}
            >
              <img src={this.props.data.url} alt="" />
            </div>
            <div
              className="glasses-info py-2 px-4 text-start"
              style={{ display: this.props.data.display }}
            >
              <h3>{this.props.data.name}</h3>
              <p>{this.props.data.desc}</p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
