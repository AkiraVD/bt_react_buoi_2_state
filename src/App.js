// import logo from "./logo.svg";
import "./css/index.css";
import "./App.css";
import GlassChangeApp from "./Components/GlassChangeApp";

function App() {
  return (
    <div className="App">
      <h2 className="header p-4">TRY GLASSES APP ONLINE</h2>
      <div>
        <GlassChangeApp />
      </div>
    </div>
  );
}

export default App;
